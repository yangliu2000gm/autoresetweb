﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.IO;
using autoresetweb.DB;



namespace autoresetweb
{
    class SoapInterface
    {
        //test environment
        //static string MeraUsername = "liuyang";
        //static string MeraPassword = "Liuyang88";


        //live
        //static string MeraUsername = "liuyang";
        //static string MeraPassword = "Liuyang201708";

        static string MeraUsername = "";
        static string MeraPassword = "";


        static SoapInterface()
        {
            //加这句 让CertificateValidation 检测总是返回true 
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            //.net 4.0以上 這麽寫
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;


            using (StreamReader sr = new StreamReader("UserAndPwd.txt"))
            {
                // Declare the dictionary outside the loop:
                var dict = new Dictionary<string, string>();

                // (This loop reads every line until EOF or the first blank line.)
                string line;
                while (!string.IsNullOrEmpty((line = sr.ReadLine())))
                {
                    // Split each line around '=':
                    var tmp = line.Split(new[] { '=' },
                                         StringSplitOptions.RemoveEmptyEntries);
                    // Add the key-value pair to the dictionary:
                    //dict[tmp[0]] = dict[tmp[1]];
                    dict.Add(tmp[0], tmp[1]);

                }

                // Assign the values that you need:
                MeraUsername = dict["userName"];
                MeraPassword = dict["pwd"];
            }





        }
        static public HttpWebRequest CreateWebRequest()
        {
            //真实server的地址     20 是业务服务器   21是镜像query服务器
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://203.223.130.21/service/service.php?soap");

            //测试server的地址
            //HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://121.121.8.153/service/service.php?soap");
            // webRequest.Headers.Add(@"SOAP:Action");
            // webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.KeepAlive = true;
            return webRequest;
        }
        //
        static public HttpWebRequest CreateWebRequestV2()
        {
            //真实server的地址     20 是业务服务器   21是镜像query服务器
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://203.223.130.21/service/service.php?soap_v2");

            //测试server的地址
            //HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://121.121.8.153/service/service.php?soap_v2");
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.KeepAlive = true;
            return webRequest;
        }

        //
        static public DataTable getAllCustomers()
        {
            HttpWebRequest request = CreateWebRequest();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                        <soapenv:Body>
                            <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                <p_table_hi xsi:type=""xsd:string"">02.2509.01</p_table_hi>
                            </soap:selectRowset>
                        </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            DataTable customerTable = new DataTable();
            customerTable.Columns.Add("clsCustomerID", typeof(string));
            customerTable.Columns.Add("clsCustomerNM", typeof(string));

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        DataRow row = customerTable.NewRow();

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "contr_hi")
                            {
                                row["clsCustomerID"] = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "contr_nm")
                            {
                                row["clsCustomerNM"] = sxe.Element("value").Value.ToString();
                            }
                        }
                        customerTable.Rows.Add(row);
                    }
                }
            }

            return customerTable;
        }

        //
        static public DataTable GetMaxisRouteStateUsingParameterPrefix(string strRoutePlanID, string strAreaID)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();


            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
   <soapenv:Body>
      <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
         <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
           <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">        
		  <type xsi:type=""xsd:string"">agg</type>
		  <operator xsi:type=""xsd:string"">and</operator>
			<childs xsi:type=""soap:filter_childs_arr"">
				<p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					<type xsi:type=""xsd:string"">cond</type>
					<column xsi:type=""xsd:string"">route_gr_hi</column>
					<operator xsi:type=""xsd:string"">=</operator>
					<value xsi:type=""xsd:string"">" + strRoutePlanID + @"</value>
				</p_filter>
				<p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					<type xsi:type=""xsd:string"">cond</type>
					<column xsi:type=""xsd:string"">prfx</column>
					<operator xsi:type=""xsd:string"">like</operator>
					<value xsi:type=""xsd:string"">" + strAreaID + @"</value>
				</p_filter>
				
			</childs>
         </p_filter>
      </soap:selectRowset>
   </soapenv:Body>
</soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            string strRouteID = null;
            string strPriority = null;
            string strAditionalParameter = null;
            string strVendor = null;
            string strVendorCost = null;
            string strEnable = null;


            DataTable table = new DataTable();
            table.Columns.Add("routeID", typeof(string));
            table.Columns.Add("priority", typeof(string));
            table.Columns.Add("aditionalParameter", typeof(string));
            table.Columns.Add("ownerVendor", typeof(string));
            table.Columns.Add("vendorCost", typeof(string));
            table.Columns.Add("enable", typeof(string));

            using (var response = request.GetResponse())
            {
                using (var rd = response.GetResponseStream())
                {
                    XmlReader reader = XmlReader.Create(rd);
                    var xd = XDocument.Load(reader);

                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "dial_peer_id")
                            {
                                strRouteID = sxe.Element("value").Value.ToString();
                            }
                            if (sxe.Element("key").Value.ToString() == "priority")
                            {
                                strPriority = sxe.Element("value").Value.ToString();
                            }
                            if (sxe.Element("key").Value.ToString() == "policy_param")
                            {
                                strAditionalParameter = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "owner_hi")
                            {
                                strVendor = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "vendor_cost")
                            {
                                strVendorCost = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "enable")
                            {
                                strEnable = sxe.Element("value").Value.ToString();
                            }

                            //owner_hi

                        }

                        table.Rows.Add(strRouteID, strPriority, strAditionalParameter, strVendor, strVendorCost, strEnable);


                    } //every row
                    //return iRtn;

                }
            }     // first using
            return table;
        }
        //
        //
        static public DataTable getAllRoutePlanOfCertainCustomer(string strCustomerID)
        {
            HttpWebRequest request = CreateWebRequest();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2513.01</p_table_hi>
                              <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                                <!--You may enter the following 5 items in any order-->
                                <type xsi:type=""xsd:string"">cond</type>
                                <column xsi:type=""xsd:string"">owner_hi</column>
                                <operator xsi:type=""xsd:string"">=</operator>
                                <value xsi:type=""xsd:string"">" + strCustomerID + @"</value>
                                <childs xsi:type=""soap:filter_childs_arr"">
                                   <!--1 or more repetitions:-->
                                   <item/>
                                </childs>
                             </p_filter>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            DataTable customerTable = new DataTable();
            customerTable.Columns.Add("clsRoutePlanID", typeof(string));
            customerTable.Columns.Add("clsRoutePlanNM", typeof(string));

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        DataRow row = customerTable.NewRow();

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "rout_gr_hi")
                            {
                                row["clsRoutePlanID"] = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "gr_nm")
                            {
                                row["clsRoutePlanNM"] = sxe.Element("value").Value.ToString();
                            }
                        }
                        customerTable.Rows.Add(row);
                    }
                }
            }

            return customerTable;
        }
        //

        static public DataTable getAllRoutePlan()
        {
            HttpWebRequest request = CreateWebRequest();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2513.01</p_table_hi>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            DataTable customerTable = new DataTable();
            customerTable.Columns.Add("clsRoutePlanID", typeof(string));
            customerTable.Columns.Add("clsRoutePlanNM", typeof(string));

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        DataRow row = customerTable.NewRow();

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "rout_gr_hi")
                            {
                                row["clsRoutePlanID"] = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "gr_nm")
                            {
                                row["clsRoutePlanNM"] = sxe.Element("value").Value.ToString();
                            }
                        }
                        customerTable.Rows.Add(row);
                    }
                }
            }

            return customerTable;
        }
        //

        static public DataTable getAllRoutePlanV2()
        {
            HttpWebRequest request = CreateWebRequestV2();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                        <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2513.01</p_table_hi>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            DataTable customerTable = new DataTable();
            customerTable.Columns.Add("clsRoutePlanID", typeof(string));
            customerTable.Columns.Add("clsRoutePlanNM", typeof(string));

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        DataRow row = customerTable.NewRow();

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("name").Value.ToString() == "rout_gr_hi")
                            {
                                row["clsRoutePlanID"] = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("name").Value.ToString() == "gr_nm")
                            {
                                row["clsRoutePlanNM"] = sxe.Element("value").Value.ToString();
                            }
                        }
                        customerTable.Rows.Add(row);
                    }
                }
            }

            return customerTable;
        }

        //
        static public void ResetRouteV2()
        {
            HttpWebRequest request = CreateWebRequestV2();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                        <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
      <soap:callContext soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
         <p_object_id xsi:type=""soap:object_id"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">02.2116.01.2101</p_object_id>
         <p_pk_rowset xsi:type=""soap:rowset"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
            <!--1 or more repetitions:-->
            <item xsi:type=""soap:row"">
               <!--1 or more repetitions:-->
               <item xsi:type=""soap:column"">
                  <!--You may enter the following 2 items in any order-->
                  <name xsi:type=""xsd:string"">dial_peer_id</name>
                  <value xsi:type=""xsd:string"">752</value>
               </item>       
            </item>
         </p_pk_rowset>
      </soap:callContext>
   </soapenv:Body>
        </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

        }

        //
        static public void ResetRouteByMultiFilterV2(List<List<string>> lssPara)
        {
            HttpWebRequest request = CreateWebRequestV2();

            string xmlRequest = null;

            if (lssPara.Count == 1)
            {
                xmlRequest = //@"<?xml version=""1.0"" encoding=""utf-8""?>
                        @"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                      <soap:callContext soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                         <p_object_id xsi:type=""soap:object_id"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">02.2116.01.2101</p_object_id>
                           <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                            <!--You may enter the following 5 items in any order-->
                            <type xsi:type=""xsd:string"">cond</type>
                            <column xsi:type=""xsd:string"">" + lssPara[0][0] + @"</column>
                            <operator xsi:type=""xsd:string"">" + lssPara[0][1] + @"</operator>
                            <value xsi:type=""xsd:string"">" + lssPara[0][2] + @"</value>
                         </p_filter>
                      </soap:callContext>
                   </soapenv:Body>
                 </soapenv:Envelope>";
            }
            else
            {
                xmlRequest = //@"<?xml version=""1.0"" encoding=""utf-8""?>
                        @"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
      <soap:callContext soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
         <p_object_id xsi:type=""soap:object_id"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">02.2116.01.2101</p_object_id>
          <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
            <!--You may enter the following 5 items in any order-->
            <type xsi:type=""xsd:string"">agg</type>
		  <operator xsi:type=""xsd:string"">and</operator>";

                string ChildFilter = @"<childs xsi:type=""soap:filter_childs_arr"">";

                foreach (List<string> ls in lssPara)
                {
                    ChildFilter = ChildFilter + @"<item>
					<type xsi:type=""xsd:string"">cond</type>
					<column xsi:type=""xsd:string"">" + ls[0] + @"</column>
					<operator xsi:type=""xsd:string"">" + ls[1] + @"</operator>
					<value xsi:type=""xsd:string"">" + ls[2] + @"</value>
				</item>";
                }
                ChildFilter = ChildFilter + @"</childs>";

                xmlRequest = xmlRequest + ChildFilter + @"</p_filter>
                              </soap:callContext>
                           </soapenv:Body>
                                </soapenv:Envelope>";
            }

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(xmlRequest);

            //先 save
            //saveXMLToDB(soapEnvelopeXml, soapEnvelopeXml);


            //再 加上 前缀 

            //XmlNode docNode = soapEnvelopeXml.CreateXmlDeclaration("1.0", "UTF-8", null);
            //soapEnvelopeXml.AppendChild(docNode);

            XmlDeclaration xmldecl;
            xmldecl = soapEnvelopeXml.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = soapEnvelopeXml.DocumentElement;
            soapEnvelopeXml.InsertBefore(xmldecl, root);





            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);

                    saveXMLToDB(soapEnvelopeXml, ToXmlDocument(xd));
                }
            }

        }
        //
        static public int UpdateRouteByMultiFilterV2(List<List<string>> lssPara)
        {
            HttpWebRequest request = CreateWebRequestV2();

            string xmlRequest = null;

            if (lssPara.Count == 1)
            {
                xmlRequest = //@"<?xml version=""1.0"" encoding=""utf-8""?>
                        @"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                      <soap:updateRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                         <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
                          <p_rowset xsi:type=""soap:rowset"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                            <!--1 or more repetitions:-->
                            <item xsi:type=""soap:row"">
                               <!--1 or more repetitions:-->
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_1</name>
                                  <value xsi:nil=""true""></value>
                               </item>
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_2</name>
                                  <value xsi:nil=""true""></value>
                               </item>
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_3</name>
                                   <value xsi:type=""xsd:string"">10</value>
                               </item>                              
                            </item>
                         </p_rowset>
                           <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                            <!--You may enter the following 5 items in any order-->
                            <type xsi:type=""xsd:string"">cond</type>
                            <column xsi:type=""xsd:string"">" + lssPara[0][0] + @"</column>
                            <operator xsi:type=""xsd:string"">" + lssPara[0][1] + @"</operator>
                            <value xsi:type=""xsd:string"">" + lssPara[0][2] + @"</value>
                         </p_filter>
                      </soap:updateRowset>
                   </soapenv:Body>
                 </soapenv:Envelope>";
            }
            else
            {
                xmlRequest = //@"<?xml version=""1.0"" encoding=""utf-8""?>
                        @"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""auth"">
                        <soapenv:Header>
  	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                      <soap:updateRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                         <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
                          <p_rowset xsi:type=""soap:rowset"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                            <!--1 or more repetitions:-->
                            <item xsi:type=""soap:row"">
                               <!--1 or more repetitions:-->
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_1</name>
                                  <value xsi:nil=""true""></value>
                               </item>
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_2</name>
                                  <value xsi:nil=""true""></value>
                               </item>
                               <item xsi:type=""soap:column"">
                                  <!--You may enter the following 2 items in any order-->
                                  <name xsi:type=""xsd:string"">appeal_value_3</name>
                                   <value xsi:type=""xsd:string"">10</value>
                               </item>                              
                            </item>
                         </p_rowset>
                <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                    <!--You may enter the following 5 items in any order-->
                    <type xsi:type=""xsd:string"">agg</type>
		          <operator xsi:type=""xsd:string"">and</operator>";

                string ChildFilter = @"<childs xsi:type=""soap:filter_childs_arr"">";

                foreach (List<string> ls in lssPara)
                {
                    ChildFilter = ChildFilter + @"<item>
					<type xsi:type=""xsd:string"">cond</type>
					<column xsi:type=""xsd:string"">" + ls[0] + @"</column>
					<operator xsi:type=""xsd:string"">" + ls[1] + @"</operator>
					<value xsi:type=""xsd:string"">" + ls[2] + @"</value>
				</item>";
                }
                ChildFilter = ChildFilter + @"</childs>";

                xmlRequest = xmlRequest + ChildFilter + @"</p_filter>
                              </soap:updateRowset>
                           </soapenv:Body>
                                </soapenv:Envelope>";
            }

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(xmlRequest);

            //先 save
            //saveXMLToDB(soapEnvelopeXml, soapEnvelopeXml);


            //再 加上 前缀 

            //XmlNode docNode = soapEnvelopeXml.CreateXmlDeclaration("1.0", "UTF-8", null);
            //soapEnvelopeXml.AppendChild(docNode);

            XmlDeclaration xmldecl;
            xmldecl = soapEnvelopeXml.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = soapEnvelopeXml.DocumentElement;
            soapEnvelopeXml.InsertBefore(xmldecl, root);





            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }


            int iRows = 0;
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    saveXMLToDB(soapEnvelopeXml, ToXmlDocument(xd));

                    foreach (XElement xe in xd.Descendants("result"))
                    {

                        iRows = int.Parse(xe.Value.ToString());
                    }

                }
            }

            return iRows;

        }


        //
        static public DataTable getAllAreas()
        {
            HttpWebRequest request = CreateWebRequest();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                        <soapenv:Body>
                            <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                <p_table_hi xsi:type=""xsd:string"">02.2506.01</p_table_hi>
                            </soap:selectRowset>
                        </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            DataTable customerTable = new DataTable();
            customerTable.Columns.Add("clsAreaID", typeof(string));
            customerTable.Columns.Add("clsAreaNM", typeof(string));

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);
                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        DataRow row = customerTable.NewRow();

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "area_name_id")
                            {
                                row["clsAreaID"] = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "area_nm")
                            {
                                row["clsAreaNM"] = sxe.Element("value").Value.ToString();
                            }
                        }
                        customerTable.Rows.Add(row);
                    }
                }
            }

            return customerTable;
        }

        //
        static public string GetPrefixArrayByAreaID(string strAreaID)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();

            string strPrefixGrp = null;

            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
               <soapenv:Body>
                  <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                     <p_table_hi xsi:type=""xsd:string"">02.2506.02</p_table_hi>
                     <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                        <type xsi:type=""xsd:string"">cond</type>
                        <column xsi:type=""xsd:string"">area_name_id</column>
                        <operator xsi:type=""xsd:string"">=</operator>
                        <value xsi:type=""xsd:string"">" + strAreaID + @"</value>
                     </p_filter>
                  </soap:selectRowset>
               </soapenv:Body>
</soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (var response = request.GetResponse())
            {
                using (var rd = response.GetResponseStream())
                {
                    XmlReader reader = XmlReader.Create(rd);
                    var xd = XDocument.Load(reader);



                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "prfx")
                            {
                                strPrefixGrp = strPrefixGrp + sxe.Element("value").Value.ToString() + @",";
                            }

                        }


                    } //every row
                    //return iRtn;

                }
            }     // first using

            if (strPrefixGrp != null)
            {
                strPrefixGrp = strPrefixGrp.Substring(0, strPrefixGrp.Length - 1);
            }

            return strPrefixGrp;
        }
        //


        //
        static public string GetRatePlanByCustomerID(string strCustomerID)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();

            string strPrefixGrp = null;

            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
               <soapenv:Body>
                  <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                     <p_table_hi xsi:type=""xsd:string"">02.2139.01</p_table_hi>
                     <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                        <type xsi:type=""xsd:string"">cond</type>
                        <column xsi:type=""xsd:string"">owner_hi</column>
                        <operator xsi:type=""xsd:string"">=</operator>
                        <value xsi:type=""xsd:string"">" + strCustomerID + @"</value>
                     </p_filter>
                  </soap:selectRowset>
               </soapenv:Body>
</soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (var response = request.GetResponse())
            {
                using (var rd = response.GetResponseStream())
                {
                    XmlReader reader = XmlReader.Create(rd);
                    var xd = XDocument.Load(reader);



                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "hi")
                            {
                                strPrefixGrp = sxe.Element("value").Value.ToString();
                            }

                        }


                    } //every row
                    //return iRtn;

                }
            }     // first using

            return strPrefixGrp;
        }
        //
        static public string GetCustomerNameFromID(string customerID)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                        <soapenv:Body>
                            <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                <p_table_hi xsi:type=""xsd:string"">02.2509.01</p_table_hi>
                              <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                                <!--You may enter the following 5 items in any order-->
                                <type xsi:type=""xsd:string"">cond</type>
                                <column xsi:type=""xsd:string"">contr_hi</column>
                                <operator xsi:type=""xsd:string"">=</operator>
                                <value xsi:type=""xsd:string"">" + customerID + @"</value>
                             </p_filter>
                            </soap:selectRowset>
                        </soapenv:Body>
                    </soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);

                    int j = 1;

                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            //dRow.Add(sxe.Element("key").Value, sxe.Element("value").Value);
                            if (sxe.Element("key").Value.ToString() == "contr_nm")
                            {
                                //File.AppendAllText(@"D:\temp\0919\test.txt", sxe.Element("key").Value.ToString() + "----" + sxe.Element("value").Value.ToString() + "\r\n");
                                return sxe.Element("value").Value.ToString();
                            }
                        }

                    } //every row
                    //File.AppendAllText(@"D:\temp\0919\test.txt", "Over...." + "\r\n");
                }
            }    //response

            return "";

        }
        //

        //
        static public string GetVendorIDFromRouteID(string routeID)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
                               <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
                                <!--You may enter the following 5 items in any order-->
                                <type xsi:type=""xsd:string"">cond</type>
                                <column xsi:type=""xsd:string"">dial_peer_id</column>
                                <operator xsi:type=""xsd:string"">=</operator>
                                <value xsi:type=""xsd:string"">" + routeID + @"</value>
                             </p_filter>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    XDocument xd = XDocument.Load(rd.BaseStream);

                    int j = 1;

                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            //dRow.Add(sxe.Element("key").Value, sxe.Element("value").Value);
                            if (sxe.Element("key").Value.ToString() == "owner_hi")
                            {
                                //File.AppendAllText(@"D:\temp\0919\test.txt", sxe.Element("key").Value.ToString() + "----" + sxe.Element("value").Value.ToString() + "\r\n");
                                return sxe.Element("value").Value.ToString();
                            }
                        }

                    } //every row
                    //File.AppendAllText(@"D:\temp\0919\test.txt", "Over...." + "\r\n");
                }
            }    //response

            return "";

        }

        //
        static public DataTable GetMaxisRouteStateUsingParameter(string strRoutePlanID, string strAreaID)
        {
            //根据 RoutePlan和area找Routes 

            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();


            //去掉 enable = 1 的条件  因为是预先设定 定时启动route , 当前可以不enable
            //            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
            //                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
            //                        <soapenv:Header>
            //   	                        <req:Login>" + MeraUsername + @"</req:Login>
            //   	                        <req:Password>" + MeraPassword + @"</req:Password>
            //                        </soapenv:Header>
            //                       <soapenv:Body>
            //                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
            //                             <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
            //                               <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">        
            //		                      <type xsi:type=""xsd:string"">agg</type>
            //		                      <operator xsi:type=""xsd:string"">and</operator>
            //			                    <childs xsi:type=""soap:filter_childs_arr"">
            //				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
            //					                    <type xsi:type=""xsd:string"">cond</type>
            //					                    <column xsi:type=""xsd:string"">route_gr_hi</column>
            //					                    <operator xsi:type=""xsd:string"">=</operator>
            //					                    <value xsi:type=""xsd:string"">" + strRoutePlanID + @"</value>
            //				                    </p_filter>
            //				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
            //					                    <type xsi:type=""xsd:string"">cond</type>
            //					                    <column xsi:type=""xsd:string"">enable</column>
            //					                    <operator xsi:type=""xsd:string"">=</operator>
            //					                    <value xsi:type=""xsd:string"">1</value>
            //				                    </p_filter>
            //				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
            //					                    <type xsi:type=""xsd:string"">cond</type>
            //					                    <column xsi:type=""xsd:string"">area_name_id</column>
            //					                    <operator xsi:type=""xsd:string"">=</operator>
            //					                    <value xsi:type=""xsd:string"">" + "a" + strAreaID + @"</value>
            //				                    </p_filter>
            //				
            //			                    </childs>
            //                             </p_filter>
            //                          </soap:selectRowset>
            //                       </soapenv:Body>
            //                    </soapenv:Envelope>");


            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2116.01</p_table_hi>
                               <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">        
		                      <type xsi:type=""xsd:string"">agg</type>
		                      <operator xsi:type=""xsd:string"">and</operator>
			                    <childs xsi:type=""soap:filter_childs_arr"">
				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					                    <type xsi:type=""xsd:string"">cond</type>
					                    <column xsi:type=""xsd:string"">route_gr_hi</column>
					                    <operator xsi:type=""xsd:string"">=</operator>
					                    <value xsi:type=""xsd:string"">" + strRoutePlanID + @"</value>
				                    </p_filter>
				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					                    <type xsi:type=""xsd:string"">cond</type>
					                    <column xsi:type=""xsd:string"">area_name_id</column>
					                    <operator xsi:type=""xsd:string"">=</operator>
					                    <value xsi:type=""xsd:string"">" + "a" + strAreaID + @"</value>
				                    </p_filter>
				
			                    </childs>
                             </p_filter>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");



            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            string strRouteID = null;
            string strPriority = null;
            string strAditionalParameter = null;
            string strVendor = null;
            string strVendorCost = null;
            string strEnable = null;

            DataTable table = new DataTable();
            table.Columns.Add("routeID", typeof(string));
            table.Columns.Add("priority", typeof(string));
            table.Columns.Add("aditionalParameter", typeof(string));
            table.Columns.Add("ownerVendor", typeof(string));
            table.Columns.Add("vendorCost", typeof(string));
            table.Columns.Add("enable", typeof(string));

            using (var response = request.GetResponse())
            {
                using (var rd = response.GetResponseStream())
                {
                    XmlReader reader = XmlReader.Create(rd);
                    var xd = XDocument.Load(reader);

                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {

                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "dial_peer_id")
                            {
                                strRouteID = sxe.Element("value").Value.ToString();
                            }
                            if (sxe.Element("key").Value.ToString() == "priority")
                            {
                                strPriority = sxe.Element("value").Value.ToString();
                            }
                            if (sxe.Element("key").Value.ToString() == "policy_param")
                            {
                                strAditionalParameter = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "owner_hi")
                            {
                                strVendor = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "vendor_cost")
                            {
                                strVendorCost = sxe.Element("value").Value.ToString();
                            }

                            if (sxe.Element("key").Value.ToString() == "enable")
                            {
                                strEnable = sxe.Element("value").Value.ToString();
                            }

                            //owner_hi
                        }

                        table.Rows.Add(strRouteID, strPriority, strAditionalParameter, strVendor, strVendorCost, strEnable);
                    } //every row
                    //return iRtn;

                }
            }     // first using
            return table;
        }

        //
        static public void GetLiveASRandACDUsingVendorIDandAreaID(string strVendorID, string strAreaID, ref string LiveASR, ref string LiveACD)
        {
            //根据 Vendor和area找 live ASR, ACD , 只找一条

            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();

            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soap=""http://mfisoft.ru/soap"" xmlns:req=""http://mfisoft.ru/auth"">
                        <soapenv:Header>
   	                        <req:Login>" + MeraUsername + @"</req:Login>
   	                        <req:Password>" + MeraPassword + @"</req:Password>
                        </soapenv:Header>
                       <soapenv:Body>
                          <soap:selectRowset soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                             <p_table_hi xsi:type=""xsd:string"">02.2271.01</p_table_hi>
                               <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">        
		                      <type xsi:type=""xsd:string"">agg</type>
		                      <operator xsi:type=""xsd:string"">and</operator>
			                    <childs xsi:type=""soap:filter_childs_arr"">
				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					                    <type xsi:type=""xsd:string"">cond</type>
					                    <column xsi:type=""xsd:string"">contr_hi</column>
					                    <operator xsi:type=""xsd:string"">=</operator>
					                    <value xsi:type=""xsd:string"">" + strVendorID + @"</value>
				                    </p_filter>
				                    <p_filter xsi:type=""soap:filter"" xmlns:soap=""http://mfisoft.ru/voip/service/soap"">
					                    <type xsi:type=""xsd:string"">cond</type>
					                    <column xsi:type=""xsd:string"">area_name_id</column>
					                    <operator xsi:type=""xsd:string"">=</operator>
					                    <value xsi:type=""xsd:string"">" + strAreaID + @"</value>
				                    </p_filter>
			                    </childs>
                             </p_filter>
                             <p_limit xsi:type=""xsd:integer"">1</p_limit>
                             <p_offset xsi:type=""xsd:integer"">0</p_offset>
                          </soap:selectRowset>
                       </soapenv:Body>
                    </soapenv:Envelope>");

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (var response = request.GetResponse())
            {
                using (var rd = response.GetResponseStream())
                {
                    XmlReader reader = XmlReader.Create(rd);
                    var xd = XDocument.Load(reader);

                    foreach (XElement xe in xd.Descendants("item").Where(p => p.HasAttributes == true))
                    {
                        foreach (XElement sxe in xe.Descendants("item"))
                        {
                            if (sxe.Element("key").Value.ToString() == "value_asr")
                            {
                                LiveASR = sxe.Element("value").Value.ToString();
                            }
                            if (sxe.Element("key").Value.ToString() == "value_acd")
                            {
                                LiveACD = sxe.Element("value").Value.ToString();
                            }
                        }

                    } //every row
                    //return iRtn;

                }
            }     // first using
            return;
        }
        //
        static public void saveXMLToDB(XmlDocument xmlRequest, XmlDocument xmlResponse)
        {

            foreach (XmlNode node in xmlRequest)
            {
                if (node.NodeType == XmlNodeType.XmlDeclaration)
                {
                    xmlRequest.RemoveChild(node);
                }
            }

            foreach (XmlNode node in xmlResponse)
            {
                if (node.NodeType == XmlNodeType.XmlDeclaration)
                {
                    xmlResponse.RemoveChild(node);
                }
            }

            //SqlConnection conn = DBFactory.getConnect();
            using (SqlConnection conn = DBFactory.getConnect())
            {
                SqlCommand sqlCmd = null;
                SqlCommand sqlCmdUpdate = null;
                try
                {
                    conn.Open();
                    sqlCmdUpdate = new SqlCommand("insert into  soapLogs select coalesce(max(id),0) + 1, @xmlRequest,@xmlResponse,@now from soapLogs ", conn);

                    sqlCmdUpdate.Parameters.Add("@xmlRequest", SqlDbType.Xml);
                    sqlCmdUpdate.Parameters["@xmlRequest"].Value = xmlRequest.OuterXml;

                    sqlCmdUpdate.Parameters.Add("@xmlResponse", SqlDbType.Xml);
                    sqlCmdUpdate.Parameters["@xmlResponse"].Value = xmlResponse.OuterXml;

                    sqlCmdUpdate.Parameters.Add("@now", SqlDbType.DateTime);
                    sqlCmdUpdate.Parameters["@now"].Value = DateTime.Now;

                    sqlCmdUpdate.CommandTimeout = 200;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        sqlCmdUpdate.ExecuteNonQuery();
                    }
                    catch (Exception aep)
                    {
                        GlobalData.logger.Info("insert error " + aep.InnerException.Message.ToString());
                        //File.AppendAllText(strCurrentPath + @"\test.txt", "insert error " + aep.Message + "\r\n");
                        return;
                    }
                }
                catch (Exception cep)
                {

                }

            }
        }
    
    

        

        //
        static public XmlDocument ToXmlDocument(XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        static public XDocument ToXDocument(XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }

        //

    }
}
