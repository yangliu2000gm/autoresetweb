using autoresetweb.Data;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using autoresetweb.DB;
using autoresetweb;
using autoresetweb.Service;

//Init ThreadPool
ThreadPool.SetMinThreads(100, 100);

GlobalData.Init();
//GlobalData.config.GetSection("Config:URL").Bind(listURL);


var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();
//XL enlarge size
builder.Services.AddServerSideBlazor().AddHubOptions(o =>
{
    o.MaximumReceiveMessageSize = 10 * 1024 * 1024;
});

//builder.Services.AddScoped<SessionState>();
builder.Services.AddSingleton<DBService>();
builder.Services.AddHostedService<TimeService>();
builder.Services.Configure<CookiePolicyOptions>(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});
builder.Services.AddAuthentication("Cookies").AddCookie();
//Blazerise Required
builder.Services
    .AddBlazorise(options =>
    {
        options.Immediate = true;
    })
    .AddBootstrapProviders()
    .AddFontAwesomeIcons();


var app = builder.Build();


app.UseStaticFiles();

app.UseRouting();
app.MapControllers();
app.MapBlazorHub();

app.UseAuthentication();
app.UseAuthorization();

app.MapFallbackToPage("/_Host");

app.Run();
//try
//{
//    app.Urls.Clear();
//    foreach (string s in listURL)
//    {
//        app.Urls.Add(s);
//    }
//    app.Run();
//}
//catch (Exception exception)
//{
//    GlobalData.logger.Fatal(exception, "Stopped program because of exception");
//    throw;
//}
//finally
//{
//    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
//    NLog.LogManager.Shutdown();
//}


