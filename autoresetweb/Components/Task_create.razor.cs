﻿using Blazorise;
using autoresetweb.DB;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components;
using System.Data;
using Blazorise.Icons.FontAwesome;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace autoresetweb.Components
{


    public partial class Task_create
    {
        public class routeplan
        {
            public string srouteplanid;
            public string srouteplanname;

        }

        private Modal modalRef;
        [Parameter]
        public EventCallback<bool> OnClose { get; set; }
        [Parameter]
        public int taskID { get; set; }
        [Parameter]
        public bool editmode { get; set; }

        [Inject] DBService dBService { get; set; }
        [Inject] IMessageService MessageService { get; set; }
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        bool alertShow = false;
        string alertMsg = "";
        int endcustomerid = 0;

        List<string> lrouteplan = new List<string>();
        TimePicker<TimeSpan?> timePicker;


        DataTable dt1 = null;

        public IEnumerable<routeplan> routeplans;
        string selectedDropValue { get; set; } = "";

        string para_name = "";
        string para_prefix = "";
        string para_routeplan = "";
        string para_yesno = "";

        protected override async Task OnInitializedAsync()
        {
            // dt1 = SoapInterface.getAllRoutePlan();

            //routeplans = dt1.AsEnumerable().Select(row => new routeplan
            //{
            //    srouteplanid = row["clsRoutePlanID"].ToString(),
            //    srouteplanname = row["clsRoutePlanNM"].ToString(),
   
            //});


        }

        public Task ShowModal()
        {
            return modalRef.Show();
        }

        private void loadFromDB()
        {

            DataTable dt = new DataTable();
            DB.DBProcedures.sp_query_task_withid(endcustomerid, ref dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    para_name = row["taskname"].ToString();

                    if (row["ColName"].ToString() == "Prefix")
                    {
                        para_prefix = row["Value"].ToString();
                    }
                    if (row["ColName"].ToString() == "routePlan")
                    {
                        para_routeplan = row["Value"].ToString();
                    }
                    if (row["ColName"].ToString() == "enable")
                    {
                        para_yesno = row["Value"].ToString();
                    }


                }



            }

        }
        public Task ShowModalEdit(int _endcustomerid)
        {
            endcustomerid = _endcustomerid;
            loadFromDB();
            StateHasChanged();
            return modalRef.Show();
        }

        private Task HideModal()
        {
            return modalRef.Hide();
        }



        void resetPara()
        {
            para_name = "";
            para_prefix = "";
            para_routeplan = "";
            para_yesno = "";

        }

        Task ModalCancel()
        {
            return OnClose.InvokeAsync(false);
        }
        async Task ModalNewOk()
        {
            if (para_name == "")
            {
                await MessageService.Error("name can not be empty");
                return;
            }


            var authState = await authenticationStateTask;
            var user = authState.User;

            if (user.Identity.IsAuthenticated)
            {
                string strMeraColName = null;
                string strDisplayColName = null;
                string strOperator = null;
                string strMeraValue = null;
                string strDisplayValue = null;

                Guid g;
                // Create and display the value of two GUIDs.
                g = Guid.NewGuid();


                if (para_prefix != "")
                {
                    strMeraColName = "prfx";
                    strDisplayColName = "Prefix";
                    strOperator = "=";
                    strMeraValue = para_prefix;
                    strDisplayValue = para_prefix;

                    using (SqlConnection conn = DBFactory.getConnect())
                    {


                        SqlCommand sqlCmd = null;
                        SqlCommand sqlCmdUpdate = null;
                        try
                        {
                        

                            sqlCmdUpdate = new SqlCommand("insert into  conditions select coalesce(max(id),0) + 1, @refid,@meraColName,@displayColName,@operator,@meraValue,@displayValue  from conditions ", conn);

                            sqlCmdUpdate.Parameters.AddWithValue("@refid", g.ToString());
                            sqlCmdUpdate.Parameters.AddWithValue("@meraColName", para_name);
                            sqlCmdUpdate.Parameters.AddWithValue("@displayColName", strDisplayColName);
                            sqlCmdUpdate.Parameters.AddWithValue("@operator", strOperator);
                            sqlCmdUpdate.Parameters.AddWithValue("@meraValue", strMeraValue);
                            sqlCmdUpdate.Parameters.AddWithValue("@displayValue", strDisplayValue);

                            sqlCmdUpdate.CommandTimeout = 200;
                            if (conn.State == ConnectionState.Closed)
                                conn.Open();
                            try
                            {
                                sqlCmdUpdate.ExecuteNonQuery();
                            }
                            catch (Exception aep)
                            {
                                // File.AppendAllText(strCurrentPath + @"\test.txt", "insert error " + aep.Message + "\r\n");
                                return;
                            }
                        }
                        catch (Exception cep)
                        {
                        }
                    }

                }  // if paraprefix not null


                if (para_routeplan != "")
                {
                    strMeraColName = "route_gr_hi";
                    strDisplayColName = "routePlan";
                    strOperator = "=";
                    strMeraValue = selectedDropValue;
                    strDisplayValue = selectedDropValue;
         

                    using (SqlConnection conn = DBFactory.getConnect())
                    {


                        SqlCommand sqlCmd = null;
                        SqlCommand sqlCmdUpdate = null;
                        try
                        {
                       

                            sqlCmdUpdate = new SqlCommand("insert into  conditions select coalesce(max(id),0) + 1, @refid,@meraColName,@displayColName,@operator,@meraValue,@displayValue  from conditions ", conn);

                            sqlCmdUpdate.Parameters.AddWithValue("@refid", g.ToString());
                            sqlCmdUpdate.Parameters.AddWithValue("@meraColName", para_name);
                            sqlCmdUpdate.Parameters.AddWithValue("@displayColName", strDisplayColName);
                            sqlCmdUpdate.Parameters.AddWithValue("@operator", strOperator);
                            sqlCmdUpdate.Parameters.AddWithValue("@meraValue", strMeraValue);
                            sqlCmdUpdate.Parameters.AddWithValue("@displayValue", strDisplayValue);

                            sqlCmdUpdate.CommandTimeout = 200;
                            if (conn.State == ConnectionState.Closed)
                                conn.Open();
                            try
                            {
                                sqlCmdUpdate.ExecuteNonQuery();
                            }
                            catch (Exception aep)
                            {
                                // File.AppendAllText(strCurrentPath + @"\test.txt", "insert error " + aep.Message + "\r\n");
                                return;
                            }
                        }
                        catch (Exception cep)
                        {
                        }
                    }

                }  // if paraprefix not null

                using (SqlConnection conn = DBFactory.getConnect())
                {
                    SqlCommand sqlCmd = null;
                    SqlCommand sqlCmdUpdate = null;
                    try
                    {
                        

                        // 确定下一次运行时间 
                        DateTime dtLastRunTime = new DateTime(2000, 1, 1, 0, 0, 0);
                        DateTime dtNextTime = DateTime.Now;
                        //TimeSpan difference = timePicker - DateTime.Now.TimeOfDay;
                        //if ((difference.Minutes < 0) || (difference.Hours < 0))
                        //{
                        //    dtNextTime = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, dtScheduler.Hour, dtScheduler.Minute, 0);
                        //}
                        //else
                        //{
                        //    dtNextTime = new DateTime(DateTime.Now.AddDays(0).Year, DateTime.Now.AddDays(0).Month, DateTime.Now.AddDays(0).Day, dtScheduler.Hour, dtScheduler.Minute, 0);
                        //}
                        dtNextTime = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, timePicker.Time.Value.Hours, timePicker.Time.Value.Minutes, 0);


                        sqlCmdUpdate = new SqlCommand("insert into  timeScheduler select coalesce(max(id),0) + 1, @invokeTime,@ifEveryday,@refid,@ifTodayAlreadyDone,@ifEnable,@taskName , @lastRunTime,@nextRunTime,@LastRunEffectRoutes   from conditions ", conn);

                        sqlCmdUpdate.Parameters.AddWithValue("@invokeTime", timePicker.Time);
                        sqlCmdUpdate.Parameters.AddWithValue("@ifEveryday", 1);
                        sqlCmdUpdate.Parameters.AddWithValue("@refid", g.ToString());
                        sqlCmdUpdate.Parameters.AddWithValue("@ifTodayAlreadyDone", 0);
                        sqlCmdUpdate.Parameters.AddWithValue("@ifEnable", 1);
                        sqlCmdUpdate.Parameters.AddWithValue("@taskName", para_name);
                        sqlCmdUpdate.Parameters.AddWithValue("@lastRunTime", dtLastRunTime);
                        sqlCmdUpdate.Parameters.AddWithValue("@nextRunTime", dtNextTime);
                        sqlCmdUpdate.Parameters.AddWithValue("@LastRunEffectRoutes", 0);

                        sqlCmdUpdate.CommandTimeout = 200;
                        if (conn.State == ConnectionState.Closed)
                            conn.Open();
                        try
                        {
                            sqlCmdUpdate.ExecuteNonQuery();
                        }
                        catch (Exception aep)
                        {
                            // File.AppendAllText(strCurrentPath + @"\test.txt", "insert error " + aep.Message + "\r\n");
                            return;
                        }
                    }
                    catch (Exception cep)
                    {

                    }
                }

            }
            else
            {
                await MessageService.Error("login first");
                return;
            }




        }
        async Task ModalEditOk()
        {
            if (para_name == "")
            {
                await MessageService.Error("name can not be empty");
                return;
            }


            var authState = await authenticationStateTask;
            var user = authState.User;

            if (user.Identity.IsAuthenticated)
            {
                //string sOperatorName = user.Claims.First().Value;
                //int iResult = 0;
                //string sMsg = "";
                //DB.DBProcedures.sp_endcustomer_modify(endcustomerid, para_name, para_isindividual, para_rescountry, para_icno,
                //    para_regcountry, para_bizregno, para_contactno, para_designation, para_email, para_address1, para_address2,
                //    para_address3, para_postcode, para_directorname, para_description, ref iResult, ref sMsg);
                //if (iResult == 0)
                //{
                //    string sLogMsg = string.Format("Name {0}, under partner id {1}", para_name, partnerID);
                //    await dBService.RecordDBLog(sOperatorName, "EDIT_END_CUSTOMER", sLogMsg);
                //    resetPara();
                //    HideModal();
                //    OnClose.InvokeAsync(true);
                //}
                //else
                //{
                //    await MessageService.Error(sMsg);
                //    return;
                //}
            }
            else
            {
                await MessageService.Error("login first");
                return;
            }


        }



    }
}
