﻿using Microsoft.AspNetCore.Components;
using System.Data;
using System.Net;

using Blazorise;
using Blazorise.DataGrid;
using autoresetweb.DB;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.Threading;

using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using Blazorise.Bootstrap;
using System.Reflection.Emit;
using Microsoft.AspNetCore.Mvc;
using autoresetweb.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;



namespace autoresetweb.Components
{
    public partial class TaskCRUD
    {
        private DBService dBService = new DBService();

        //List<List<ComboBox>> llb = new List<List<ComboBox>>();
        List<ModelAllTasks> dataModelsAllTask = new List<ModelAllTasks>();
        ModelAllTasks selectedTask;


        string task_name ="";
        
        List<string> task_operators = new List<string>
            { "=", "like" };

        List<string> task_conditions = new List<string>
            { "Prefix", "Area","RoutePlan" ,"Enable"};

        Task_create dlgCreateNew;
        Task_create dlgEdit;

        private Task OnConditionChanged(ChangeEventArgs e)
        {
            string s1 = e.Value.ToString();

            //switch (s1)
            //{
            //    case "Prefix":
            //        try
            //        {
            //            if (int.Parse(combo.Tag.ToString()) > 1)
            //            {
            //                int iIndex = int.Parse(combo.Tag.ToString());
            //                llb[iIndex - 1][2].DataSource = null;
            //            }
            //            else
            //            {
            //                comboBox3.DataSource = null;

            //            }
            //        }
            //        catch (WebException lwep)
            //        {
            //            MessageBox.Show("please confirm if your machine have the permissions to visit Mera living servers . ");
            //            this.Close();
            //        }
            //        break;
            //    case "Area":
            //        try
            //        {
            //            DataTable dt1 = SoapInterface.getAllAreas();
            //            if (int.Parse(combo.Tag.ToString()) > 1)
            //            {
            //                int iIndex = int.Parse(combo.Tag.ToString());
            //                llb[iIndex - 1][2].DataSource = dt1;
            //                llb[iIndex - 1][2].DisplayMember = "clsAreaNM";
            //                llb[iIndex - 1][2].ValueMember = "clsAreaID";
            //            }
            //            else
            //            {
            //                comboBox3.DataSource = dt1;
            //                comboBox3.DisplayMember = "clsAreaNM";
            //                comboBox3.ValueMember = "clsAreaID";
            //            }
            //        }
            //        catch (WebException lwep)
            //        {
            //            MessageBox.Show("please confirm if your machine have the permissions to visit Mera living servers . ");
            //            this.Close();
            //        }
            //        break;
            //    case "RoutePlan":
            //        try
            //        {
            //            DataTable dt2 = SoapInterface.getAllRoutePlan();
            //            if (int.Parse(combo.Tag.ToString()) > 1)
            //            {
            //                int iIndex = int.Parse(combo.Tag.ToString());
            //                llb[iIndex - 1][2].DataSource = dt2;
            //                llb[iIndex - 1][2].DisplayMember = "clsRoutePlanNM";
            //                llb[iIndex - 1][2].ValueMember = "clsRoutePlanID";
            //            }
            //            else
            //            {
            //                comboBox3.DataSource = dt2;
            //                comboBox3.DisplayMember = "clsRoutePlanNM";
            //                comboBox3.ValueMember = "clsRoutePlanID";
            //            }
            //        }
            //        catch (WebException lwep)
            //        {
            //            MessageBox.Show("please confirm if your machine have the permissions to visit Mera living servers . ");
            //            this.Close();
            //        }
            //        break;
            //    case "Enable":
            //        try
            //        {
            //            //DataTable dt3 = new DataTable();
            //            DataTable dt = new DataTable();
            //            dt.Clear();
            //            dt.Columns.Add("display");
            //            dt.Columns.Add("value");
            //            DataRow _ravi = dt.NewRow();
            //            _ravi["display"] = "yes";
            //            _ravi["value"] = "1";
            //            dt.Rows.Add(_ravi);
            //            DataRow _ravi1 = dt.NewRow();
            //            _ravi1["display"] = "no";
            //            _ravi["value"] = "0";
            //            dt.Rows.Add(_ravi1);



            //            if (int.Parse(combo.Tag.ToString()) > 1)
            //            {
            //                int iIndex = int.Parse(combo.Tag.ToString());
            //                llb[iIndex - 1][2].DataSource = dt;
            //                llb[iIndex - 1][2].DisplayMember = "display";
            //                llb[iIndex - 1][2].ValueMember = "value";
            //            }
            //            else
            //            {
            //                comboBox3.DataSource = dt;
            //                comboBox3.DisplayMember = "display";
            //                comboBox3.ValueMember = "value";
            //            }
            //        }
            //        catch (WebException lwep)
            //        {
            //            MessageBox.Show("please confirm if your machine have the permissions to visit Mera living servers . ");
            //            this.Close();
            //        }
            //        break;
            //    default:
            //        break;
            //}
            return null;
        }

        async Task Edit()
        {
            if (selectedTask == null)
            {
                
                return;
            }
            dlgEdit.ShowModalEdit(selectedTask.id);
        }

        async Task DlgClose_New2(bool value)
        {
            if (value)
                RefreshData();
            else
                StateHasChanged();
        }
        async Task DlgClose_Edit2(bool value)
        {
            if (value)
                RefreshData();
            else
                StateHasChanged();
        }


        protected override async Task OnInitializedAsync()
        {

            await RefreshData();

        }
        async Task RefreshData()
        {


            dataModelsAllTask =  dBService.GetAllTasks();

            StateHasChanged();
        }


    }
}
