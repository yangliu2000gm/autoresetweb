﻿using Blazorise;
using Blazorise.DataGrid;
using autoresetweb.DB;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.Threading;

using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using Blazorise.Bootstrap;
using System.Reflection.Emit;
using Microsoft.AspNetCore.Mvc;
using autoresetweb.Service;

namespace autoresetweb.Components
{
    public partial class TaskStatus
    {
        System.Threading.Timer _timer;
        bool startEnable = true;
        bool stopEnable = true;

        List<ModelNextTasks> dataModelsNextTask = new List<ModelNextTasks>();
        List<ModelAllTasks> dataModelsAllTask = new List<ModelAllTasks>();


        String labeltext = "";
        [Inject] DBService dBService { get; set; }
        [Inject] IMessageService MessageService { get; set; }
        [Inject] NavigationManager NavigationManager { get; set; }
        [Inject] IJSRuntime JS { get; set; }

        protected override async Task OnInitializedAsync()
        {

            await RefreshData();

        }

        async Task OnStartClicked()
        {
            _timer = new System.Threading.Timer(new TimerCallback(JobCallBack), null, 0, 60000);

            TimeService.IsRunning = true;
        }

        async Task OnStopClicked()
        {
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
       
            }

            TimeService.IsRunning = false;
        }


        async void JobCallBack(object state)
        {

            labeltext = @"Last query time is : " + TimeService.lastquery.ToString() + "\r\n" + @"Below is next task 's condition";






            // stop timer , to avoid re-enter thread function 
            if (_timer != null)
            {
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
            }

            dataModelsNextTask =  dBService.GetNextTasks();
            dataModelsAllTask = dBService.GetAllTasks();

            await InvokeAsync(StateHasChanged);

            //            DateTime dtNextTaskTime = DateTime.Parse(dataModelsNextTask[0].nextrun_time.ToString());    // nextRunTime 

            //            TimeSpan difference = dtNextTaskTime - DateTime.Now;
            //            if (Math.Abs(difference.TotalMinutes) < 5)   // 等于 0 ,就是正好到时间   ，  执行reset 
            //            {
            //                if (dataModelsNextTask.Count > 0)
            //                {
            //                    // 按 id 分组 ， 每组调用一次 soapInterface 
            //                    List<List<string>> lssPara = new List<List<string>>();

            //                    int iID = int.Parse(dataModelsNextTask[0].id.ToString());    // shceduler ID 

            //                    foreach (ModelNextTasks row in dataModelsNextTask) // Loop over the rows.
            //                    {
            //                        int iThisRowID = int.Parse(row.id.ToString());           // multi row can have same "id" , each row represent one condition  , task can have multi conditions  
            //                        if (iThisRowID == iID)
            //                        {
            //                            List<string> ls = new List<string>();
            //                            ls.Add(row.meracol_name.ToString());
            //                            ls.Add(row.operator_name.ToString());
            //                            ls.Add(row.mera_value.ToString());
            //                            lssPara.Add(ls);
            //                        }
            //                        else    // not only one ID 
            //                        {

            //#if LOCALDEBUG
            //                                    DammyupdateScheduler(iID, 1);   //iRet

            //#else

            //                            SoapInterface.ResetRouteByMultiFilterV2(lssPara);             //20180423 temp
            //                            int iRet11 = SoapInterface.UpdateRouteByMultiFilterV2(lssPara);
            //                            //更新lastRunTime and 
            //                            await updateScheduler(iID, iRet11);   //iRet
            //#endif

            //                            iID = iThisRowID;
            //                            lssPara.Clear();
            //                            List<string> ls = new List<string>();
            //                            ls.Add(row.meracol_name.ToString());
            //                            ls.Add(row.operator_name.ToString());
            //                            ls.Add(row.mera_value.ToString());
            //                            lssPara.Add(ls);
            //                        }
            //                    }
            //#if LOCALDEBUG
            //                             DammyupdateScheduler(iID, 1);   //iRet
            //#else


            //                    // when end all rows , send to soapInterface 
            //                    SoapInterface.ResetRouteByMultiFilterV2(lssPara);    //20180423 temp
            //                    int iRet = SoapInterface.UpdateRouteByMultiFilterV2(lssPara);   //reset 影响的行数 
            //                                                                                    //更新lastRunTime and 
            //                    await updateScheduler(iID, iRet);
            //#endif
            //                }


            //                if (_timer != null)
            //                {
            //                    //_timer.Change(300000, 60000);
            //                    _timer.Change(60000, 60000);
            //                }
            //                return;
            //            }     // 小于三分钟 

            // restart timer 
            if (_timer != null)
            {
                _timer.Change(60000, 60000);
            }



        }

        async Task RefreshData()
        {

            //dataModelsNextTask = await dBService.GetNextTasks();
            //dataModelsAllTask = await DBService.GetAllTasks();

            StateHasChanged();
        }



    }
}
