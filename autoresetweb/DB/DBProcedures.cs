﻿using System.Data;
using System.Data.SqlClient;


namespace autoresetweb.DB
{
    public class DBProcedures
    {
        static public int TimeOutSec = 0;

        public static void sp_query_netxrun_tasks(ref DataTable dt)
        {
            dt = new DataTable();
            SqlDataReader reader = null;
            SqlConnection conn = DBFactory.getConnect();
            SqlCommand sqlCmd = new SqlCommand("sp_query_netxrun_tasks", conn);
            sqlCmd.CommandTimeout = TimeOutSec;
            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                reader = sqlCmd.ExecuteReader();
                if (reader != null)
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                GlobalData.logger.Error("Execute Procedure 'sp_query_netxrun_tasks' Failed, Error Message : '" + ex.Message + "'.");
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
                sqlCmd.Dispose();
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public static void sp_query_task_withid(int taskid, ref DataTable dt)
        {
            dt = new DataTable();
            SqlDataReader reader = null;
            SqlConnection conn = DBFactory.getConnect();
            SqlCommand sqlCmd = new SqlCommand("sp_query_task_withid", conn);
            sqlCmd.CommandTimeout = TimeOutSec;
            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@taskid", SqlDbType.Int, 4);
                sqlCmd.Parameters["@taskid"].Direction = ParameterDirection.Input;
                sqlCmd.Parameters["@taskid"].Value = taskid;
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                reader = sqlCmd.ExecuteReader();
                if (reader != null)
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                GlobalData.logger.Error("Execute Procedure 'sp_query_task_withid' Failed, Error Message : '" + ex.Message + "'.");
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
                sqlCmd.Dispose();
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }


        public static void sp_query_all_tasks(ref DataTable dt)
        {
            dt = new DataTable();
            SqlDataReader reader = null;
            SqlConnection conn = DBFactory.getConnect();
            SqlCommand sqlCmd = new SqlCommand("sp_query_all_tasks", conn);
            sqlCmd.CommandTimeout = TimeOutSec;
            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                reader = sqlCmd.ExecuteReader();
                if (reader != null)
                {
                    dt.Load(reader);
                }
            }
            catch (Exception ex)
            {
                GlobalData.logger.Error("Execute Procedure 'sp_query_all_tasks' Failed, Error Message : '" + ex.Message + "'.");
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
                sqlCmd.Dispose();
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

    }
}
