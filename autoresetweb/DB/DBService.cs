﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Metrics;

namespace autoresetweb.DB
{
    public class DBService
    {
        public List<ModelNextTasks> GetNextTasks()
        {
            List<ModelNextTasks> result = new List<ModelNextTasks>();
            DataTable dt = new DataTable();
            DB.DBProcedures.sp_query_netxrun_tasks(ref dt);
            foreach (DataRow dr in dt.Rows)
            {
                ModelNextTasks d = new ModelNextTasks();
                d.id = int.Parse(dr["id"].ToString());
                d.invoke_time = DateTime.Parse(dr["invokeTime"].ToString());
                d.diffValue = int.Parse(dr["diffValue"].ToString());
                d.operator_name = dr["operator"].ToString();
                d.displayCol_name = dr["displayColName"].ToString();
                d.display_value = dr["displayValue"].ToString();

                d.lastrun_time = DateTime.Parse(dr["lastRunTime"].ToString());
                d.nextrun_time = DateTime.Parse(dr["nextRunTime"].ToString());
                d.ifeveryday = int.Parse(dr["ifEveryday"].ToString());
                d.meracol_name = dr["meraColName"].ToString();
                d.mera_value = dr["meraValue"].ToString();



                result.Add(d);
            }
            return result;
        }


        public  List<ModelAllTasks> GetAllTasks()
        {
            List<ModelAllTasks> result = new List<ModelAllTasks>();
            DataTable dt = new DataTable();
            DB.DBProcedures.sp_query_all_tasks(ref dt);
            foreach (DataRow dr in dt.Rows)
            {
                ModelAllTasks d = new ModelAllTasks();
                d.id = int.Parse(dr["ID"].ToString());
                d.task_time = DateTime.Parse(dr["TaskTime"].ToString()); 
                d.operator_name = dr["Operator"].ToString();
                d.displayCol_name = dr["ColName"].ToString();
                d.display_value = dr["Value"].ToString();

                d.lastrun_time = DateTime.Parse(dr["LastRunTime"].ToString());
                d.nextrun_time = DateTime.Parse(dr["NextRunTime"].ToString());
                d.lastrun_effectroutes = int.Parse(dr["LastRunEffectRoutes"].ToString());
                d.displayValue = dr["displayValue"].ToString();




                result.Add(d);
            }
            return result;
        }

    }
}
