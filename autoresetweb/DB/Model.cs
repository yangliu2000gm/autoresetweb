﻿namespace autoresetweb.DB
{
    public class ModelNextTasks
    {
        public int id { set; get; } = 0;
        public string task_name { set; get; } = "";
        
        public DateTime invoke_time { set; get; } = DateTime.Today;

        public int diffValue { set; get; } = 0;

        public string displayCol_name { set; get; } = "";
        public string operator_name { set; get; } = "";
        public string display_value { set; get; } = "";
        public DateTime lastrun_time { set; get; } = DateTime.Today;
        public DateTime nextrun_time { set; get; } = DateTime.Today;
        public int ifeveryday { set; get; } = 0;
        public string meracol_name { set; get; } = "";
        public string mera_value { set; get; } = "";


    }


    public class ModelAllTasks
    {
        public int id { set; get; } = 0;
        public DateTime task_time { set; get; } = DateTime.Today;


        public string displayCol_name { set; get; } = "";
        public string operator_name { set; get; } = "";
        public string display_value { set; get; } = "";
        public DateTime lastrun_time { set; get; } = DateTime.Today;
        public DateTime nextrun_time { set; get; } = DateTime.Today;
        public int lastrun_effectroutes { set; get; } = 0;
        public string displayValue { set; get; } = "";


    }

}
