﻿using autoresetweb.DB;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Diagnostics;

namespace autoresetweb.Service
{
    public class TimeService : IHostedService, IDisposable
    {
        public static bool IsRunning = true; 
        private int executionCount = 0;
        private readonly ILogger<TimeService> _logger;
        private Timer? _timer = null;
        static public DateTime lastquery = DateTime.Now;

        private DBService dBService = new DBService();

        public TimeService(ILogger<TimeService> logger)
        {
            _logger = logger;
            IsRunning = true;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(300));

            return Task.CompletedTask;
        }


        private void DoWork(object? state)
        {

            GlobalData.logger.Info(DateTime.Now.ToString());



            if (IsRunning)
            {

                List<ModelNextTasks> dataModelsNextTask = dBService.GetNextTasks();
                //List<ModelAllTasks> dataModelsAllTask = dBService.GetAllTasks();

                if (dataModelsNextTask.Count <= 0)
                    return;

                DateTime dtNextTaskTime = DateTime.Parse(dataModelsNextTask[0].nextrun_time.ToString());    // nextRunTime 

                TimeSpan difference = dtNextTaskTime - DateTime.Now;
                if (Math.Abs(difference.TotalMinutes) < 5)   // 等于 0 ,就是正好到时间   ，  执行reset 
                {
                    if (dataModelsNextTask.Count > 0)
                    {
                        if (_timer != null)
                        {
                            _timer.Change(Timeout.Infinite, Timeout.Infinite);
                        }


                        GlobalData.logger.Info("will call mera soap interface");
                        // 按 id 分组 ， 每组调用一次 soapInterface 
                        List<List<string>> lssPara = new List<List<string>>();

                        int iID = int.Parse(dataModelsNextTask[0].id.ToString());    // shceduler ID 

                        foreach (ModelNextTasks row in dataModelsNextTask) // Loop over the rows.
                        {
                            int iThisRowID = int.Parse(row.id.ToString());           // multi row can have same "id" , each row represent one condition  , task can have multi conditions  
                            if (iThisRowID == iID)
                            {
                                List<string> ls = new List<string>();
                                ls.Add(row.meracol_name.ToString());
                                ls.Add(row.operator_name.ToString());
                                ls.Add(row.mera_value.ToString());
                                lssPara.Add(ls);
                            }
                            else    // not only one ID 
                            {

#if LOCALDEBUG
                                    DammyupdateScheduler(iID, 1);   //iRet

#else
                                try
                                {
                                    GlobalData.logger.Info("find new task , will call mera api for last ID : " + iID.ToString());

                                    SoapInterface.ResetRouteByMultiFilterV2(lssPara);             //20180423 temp
                                    int iRet11 = SoapInterface.UpdateRouteByMultiFilterV2(lssPara);
                                    //更新lastRunTime and 
                                    updateScheduler(iID, iRet11);   //iRet
                                }
                                catch (Exception e)
                                {
                                    GlobalData.logger.Info("raise exception" + e.InnerException.Message.ToString());
                                }
#endif

                                iID = iThisRowID;
                                lssPara.Clear();
                                List<string> ls = new List<string>();
                                ls.Add(row.meracol_name.ToString());
                                ls.Add(row.operator_name.ToString());
                                ls.Add(row.mera_value.ToString());
                                lssPara.Add(ls);
                            }
                        }
#if LOCALDEBUG
                             DammyupdateScheduler(iID, 1);   //iRet
#else
                        int iRet = 0;
                        try
                        {
                            // when end all rows , send to soapInterface 
                            GlobalData.logger.Info("now the last task , task ID : " + iID.ToString());

                            SoapInterface.ResetRouteByMultiFilterV2(lssPara);    //20180423 temp
                        }
                        catch (Exception ae)
                        {
                            GlobalData.logger.Info("reset raise exception" + ae.InnerException.Message.ToString());
                        }

                        try { 
                             iRet = SoapInterface.UpdateRouteByMultiFilterV2(lssPara);   //reset 影响的行数 
                                                                                            //更新lastRunTime and 
                           
                        }
                        catch (Exception e)
                        {
                            GlobalData.logger.Info("query effet count raise exception" + e.InnerException.Message.ToString());
                        }

                        updateScheduler(iID, iRet);

#endif
                        if (_timer != null)
                        {
                            _timer.Change(TimeSpan.FromSeconds(300), TimeSpan.FromSeconds(300));
                        }

                    }

                }
            }
            GlobalData.logger.Info("timer done work");


        }

        public void   updateScheduler(int iID, int routes)
        {
            DateTime dtnow = DateTime.Now;

            File.AppendAllText(@"MyLog.txt", dtnow.ToString() + " will update id " + iID.ToString() + "\r\n");
            string ConnSQL = GlobalData.config["Config:SQLConn"].ToString();

            using (SqlConnection conn = new SqlConnection(ConnSQL))
            {
                SqlCommand sqlCmd = null;
                SqlCommand sqlCmdUpdate = null;
                try
                {
                    conn.Open();

                    //                    sqlCmdUpdate = new SqlCommand(@"update timeScheduler set lastRunTime = @lt ,
                    //                                                                                nextRunTime = DATEADD(day, 1, @lt ),
                    //                                                                                 ifTodayAlreadyDone=1,
                    //                                                                                 LastRunEffectRoutes = @routes
                    //                                                                                where id =@id", conn);


                    sqlCmdUpdate = new SqlCommand(@"update timeScheduler set lastRunTime = @lt ,
                                                                                nextRunTime = DATEADD(day, 1,nextRunTime),
                                                                                 ifTodayAlreadyDone=1,
                                                                                 LastRunEffectRoutes = @routes
                                                                                where id =@id", conn);



                    //nextRunTime = DATEADD(day, 1, lastRunTime),

                    sqlCmdUpdate.Parameters.AddWithValue("@lt", dtnow);
                    sqlCmdUpdate.Parameters.AddWithValue("@routes", routes);
                    sqlCmdUpdate.Parameters.AddWithValue("@id", iID);

                    sqlCmdUpdate.CommandTimeout = 200;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        sqlCmdUpdate.ExecuteNonQuery();
                    }
                    catch (Exception aep)
                    {
                        File.AppendAllText(@"MyLog.txt", DateTime.Now.ToString() + " error update id " + iID.ToString() + "\r\n");
                        if (_timer != null)
                        {
                            _timer.Change(300, 300);
                        }
                        return;
                    }

                }
                catch (Exception cep)
                {
                    File.AppendAllText(@"MyLog.txt", DateTime.Now.ToString() + " exception happen  " + cep.Message + "\r\n");
                }
            }   // end of using 

        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}