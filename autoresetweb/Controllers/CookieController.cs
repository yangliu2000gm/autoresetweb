﻿
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System.Security.Claims;
    //using static BlazorSvrExample.CommonTools;
    //using BlazorSvrExample.DB;


    namespace autoresetweb.Controllers
    {
        [Route("/[controller]")]
        [ApiController]
        public class CookieController : ControllerBase
        {
            [HttpPost]
            public async Task<ActionResult> Login([FromForm] string name, [FromForm] string password)
            {
                int iResult = 0;
                int iCustID = 0;
                string sMsg = "";


                ClaimsIdentity claimsIdentity = new ClaimsIdentity(new List<Claim>
                     {
                         new Claim(ClaimTypes.NameIdentifier, name),
                         new Claim(ClaimTypes.Role, "admin"),
                         new Claim(ClaimTypes.Email, iCustID.ToString()),
                     }, name);
                ClaimsPrincipal claims = new ClaimsPrincipal(claimsIdentity);
                await HttpContext.SignInAsync(claims);


                return Redirect("/taskstatus");


            //DB.DBProcedures.sp_admin_login(name, password, ref iCustID, ref iResult, ref sMsg);
            //if (iResult == 0)
            //{
            //    DB.DBProcedures.sp_customer_oplog_create(iCustID, "LOGIN_SUCCESS", "", ref iResult, ref sMsg);

            //    ClaimsIdentity claimsIdentity = new ClaimsIdentity(new List<Claim>
            // {
            //     new Claim(ClaimTypes.NameIdentifier, name),
            //     new Claim(ClaimTypes.Role, "admin"),
            //     new Claim(ClaimTypes.Email, iCustID.ToString()),
            // }, name);
            //    ClaimsPrincipal claims = new ClaimsPrincipal(claimsIdentity);
            //    await HttpContext.SignInAsync(claims);
            //    return Redirect("/");
            //}
            //else
            //    return Redirect("/loginerror");

        }


            //[HttpPost("ResetForgotPassword")]
            //public async Task<IActionResult> ResetForgotPassword(ResetPasswordDTO model)
            //{
            //    try
            //    {
            //        var email = new MimeMessage();
            //        email.From.Add(MailboxAddress.Parse(config.GetSection("EmailUsername").Value));
            //        email.To.Add(MailboxAddress.Parse(request.To));

            //        email.Subject = request.Subject;
            //        email.Body = new TextPart(TextFormat.Html) { Text = request.Message };

            //        using var smtp = new SmtpClient();
            //        smtp.Connect(config.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);

            //        smtp.Authenticate(config.GetSection("EmailUsername").Value, config.GetSection("EmailPassword").Value);
            //        smtp.Send(email);
            //        smtp.Disconnect(true);
            //        return "Mail Sent!";
            //    }
            //    catch (Exception)
            //    {

            //        throw;
            //    }


            //}




            [HttpGet("logout")]
            public async Task<ActionResult> Logout()
            {

                await HttpContext.SignOutAsync("Cookies");
                return Redirect("/");
            }
        }
    

}
